This is the data source for the program in:
MCDB - Bardwell - GA

This repo will not contain any of the processing program. It is setup to easily share the data files.

The code for processing data is all contained in the repo at: 
https://bitbucket.org/umarsdev/mcdb-bardwell-ga
